package com.puravida.gradle.groogle

import org.gradle.api.Action
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.artifacts.Configuration
import org.gradle.api.artifacts.ResolvableDependencies

class GrooglePlugin implements Plugin<Project> {

    static final String GROOGLE = 'groogle'
    static final String GROOGLE_CONFIG = 'groogleConfig'

    static final String GOOGLE_CORE_DEPENDENCY = 'com.google.api-client:google-api-client:'
    static final String GOOGLE_OAUTH_DEPENDENCY = 'com.google.oauth-client:google-oauth-client-jetty:'

    void apply(Project project) {

        project.with{
            apply(plugin:'base')

            GrooglePluginExtension extension = extensions.create(GROOGLE_CONFIG, GrooglePluginExtension, project)

            project.afterEvaluate {
            }

            Configuration configuration = configurations.maybeCreate(GROOGLE)
            logger.info("[Groogle] version ")

            configuration.incoming.beforeResolve(new Action<ResolvableDependencies>() {
                @SuppressWarnings('UnusedMethodParameter')
                void execute(ResolvableDependencies resolvableDependencies) {
                }
            })

            tasks.create('fromDrive', FromDriveTask, new Action<FromDriveTask>() {
                @Override
                void execute(FromDriveTask fromDriveTask) {
                    fromDriveTask.group = 'Google'
                    fromDriveTask.description='Look Mom, a Google DSL in my Gradle'
                    fromDriveTask.credentials = extension.credentials
                }
            })

        }


    }
}