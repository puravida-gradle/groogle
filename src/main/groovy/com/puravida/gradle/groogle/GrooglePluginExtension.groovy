package com.puravida.gradle.groogle

import org.gradle.api.Project
import org.gradle.api.provider.Property
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.InputFile
import org.gradle.api.tasks.Internal

class GrooglePluginExtension {

    private final Property<Object> credentials

    GrooglePluginExtension(Project project) {
        credentials = project.getObjects().property(Object)
    }

    Property<Object> getCredentials() {
        credentials
    }

    /*
        File f = project.file(clientSecret)
        if( f.exists()  ){
            return f.newInputStream()
        }else{
            InputStream resource = this.class.getResourceAsStream(clientSecret.toString())
            if( resource ){
                return resource
            }else{
                return new ByteArrayInputStream(clientSecret.toString().getBytes('UTF-8'))
            }
        }
    */

}
