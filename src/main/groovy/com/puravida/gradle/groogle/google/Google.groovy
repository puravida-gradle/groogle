package com.puravida.gradle.groogle.google

import com.google.api.client.auth.oauth2.Credential
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport
import com.google.api.client.http.HttpTransport
import com.google.api.client.json.JsonFactory
import com.google.api.client.json.jackson2.JacksonFactory
import com.google.api.services.drive.Drive
import com.google.api.services.drive.model.File

class Google {

    private JsonFactory jsonFactory = JacksonFactory.defaultInstance
    private HttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport()

    private Credential credential

    Credential login( InputStream clientSecret, List<String> scopes){
        credential = GoogleCredential.fromStream(clientSecret).createScoped(scopes)
    }

    void withFiles(String folderId, Closure cl, String path=""){
        Drive driveService = new Drive.Builder(httpTransport, jsonFactory, credential)
                .setApplicationName('groogle-gradle')
                .build()

        def request = driveService.files().list()
        request.q = "'$folderId' in parents"
        request.fields="nextPageToken,files(id,name,parents,mimeType)"
        def response = request.execute()
        while (true){
            response.files.each{ File file->
                if( file.mimeType == 'application/vnd.google-apps.folder' ) {
                    withFiles(file.id, cl, "$path/$file.name")
                }else{
                    cl(file, path)
                }
            }
            if(!response.nextPageToken)
                break
            request.pageToken = response.nextPageToken
            response = request.execute()
        }
    }

    void writeFile( String fileId, java.io.File file){
        Drive driveService = new Drive.Builder(httpTransport, jsonFactory, credential)
                .setApplicationName('groogle-gradle')
                .build()

        OutputStream outputStream = new ByteArrayOutputStream()
        driveService.files().get(fileId).executeMediaAndDownloadTo(outputStream)
        file.bytes = outputStream.toByteArray()

    }

}
