package com.puravida.gradle.groogle

import com.puravida.gradle.groogle.google.Google
import org.gradle.api.DefaultTask
import org.gradle.api.artifacts.Configuration
import org.gradle.api.provider.Property
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.InputFiles
import org.gradle.api.tasks.Internal
import org.gradle.api.tasks.Optional
import org.gradle.api.tasks.OutputDirectory
import org.gradle.api.tasks.TaskAction

class FromDriveTask extends DefaultTask{

    @Internal
    Google google = new Google()

    FromDriveTask(){
        credentials = project.objects.property(Object)
    }

    private login(List<String>scopes){
        InputStream io
        Object obj = credentials.get()
        File f = project.file(obj)
        if( f.exists() ) {
            io = f.newInputStream()
        }else{
            String env = System.getenv(obj.toString())
            if( !env ){
                env = obj.toString()
            }
            io = new ByteArrayInputStream(env.getBytes('UTF-8'))
        }
        google.login( io, scopes)
    }

    private final Property<Object> credentials

    @Input
    Property<Object>getCredentials(){
        credentials
    }

    private String folderId

    @Input
    String getFrom(){
        folderId
    }

    void setFrom(Object from){
        folderId = from.toString()
    }

    void from(Object from){
        folderId = from.toString()
    }

    @OutputDirectory
    File getInto(){
        project.file(intoDir)
    }

    private Object intoDir

    void setInto(Object into){
        this.intoDir = into
    }

    void into(Object into){
        this.intoDir = into
    }


    @TaskAction
    void copyFilesFromDrive(){
        assert folderId, "specify a remote folder with 'from' "

        login(["https://www.googleapis.com/auth/drive"])
        google.withFiles folderId, { file, path ->
            File dir = new File(path, into)
            dir.mkdirs()
            File out = new File(file.name, dir)
            google.writeFile(file.id, out)
        }
    }

}
