package com.puravida.gradle.groogle

import groovy.util.logging.Log
import org.gradle.testkit.runner.GradleRunner
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import spock.lang.Specification
import static org.gradle.testkit.runner.TaskOutcome.SUCCESS

@Log
class FromDriveTaskSpec extends Specification{

    @Rule
    TemporaryFolder testProjectDir

    File buildFile

    def setup() {
        new File("credentials.json", testProjectDir.root).text = new File("credentials.json").text

        buildFile = testProjectDir.newFile('build.gradle')
        buildFile << """

// tag::addPlugin[]

            plugins {
                id 'com.puravida.gradle.groogle'
            }
            
            groogleConfig{
                credentials = 'GROOGLE_CREDENTIALS' // <1>
                
                credentials = '{"type": "service_account", "project_id": ...}' //<2>

                credentials = file('credentials.json')  // <3>
                
                credentials = 'credentials.json'    // <4>
            }
            
// end::addPlugin[]

            """
    }

    def "Copy files from drive"() {
        buildFile << """

// tag::taskFromDrive[]
            fromDrive{
                from '1-amEeJa6KCVJbF_oMdSacf-NlCiIOWCu'    //<1>
                into file('docs')                           //<2>       
                into 'docs'                                 //<3> 
            }
// end::taskFromDrive[]
            
        """

        when:
        def result = GradleRunner.create()
                .withProjectDir(testProjectDir.root)
                .withArguments('fromDrive', '--stacktrace')
                .withPluginClasspath()
                .withDebug(true)
                .build()

        then:
        result.task(":fromDrive").outcome == SUCCESS

        and:
        new File('docs',testProjectDir.root).listFiles().size()
    }

}
