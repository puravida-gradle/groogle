package com.puravida.gradle.groogle


import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.api.artifacts.DependencyResolutionListener
import org.gradle.api.artifacts.ResolvableDependencies
import org.gradle.testfixtures.ProjectBuilder
import spock.lang.Ignore
import spock.lang.Specification

class GrooglePluginSpec extends Specification{

    Project project

    def setup() {
        project = ProjectBuilder.builder().build()
    }

    @SuppressWarnings('MethodName')
    def "Applies plugin and checks default setup"() {

        expect:
        project.tasks.findByName("fromDrive") == null

        when:
        project.apply plugin: 'com.puravida.gradle.groogle'

        then:
        Task fromDrive = project.tasks.findByName('fromDrive')
        fromDrive != null
        fromDrive.group == 'Google'

        project.tasks.findByName('clean') != null
    }


}
